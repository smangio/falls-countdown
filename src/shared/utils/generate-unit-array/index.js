import _ from 'lodash';

const generateUnitArray = (time) => {
  return [
    {
      value: time.days,
      text: !_.isEqual(time.days, 1) ? 'DAYS' : 'DAY',
    },
    {
      value: time.hours,
      text: !_.isEqual(time.hours, 1) ? 'HOURS' : 'HOUR',
    },
    {
      value: time.minutes,
      text: !_.isEqual(time.minutes, 1) ? 'MINS' : 'MIN',
    },
    {
      value: time.seconds,
      text: !_.isEqual(time.seconds, 1) ? 'SECS' : 'SEC',
    }
  ];
};

export default generateUnitArray;
