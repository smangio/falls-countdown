const FALLS_DATE_JS_OBJ = new Date(2018, 11, 28, 16);
const FALLS_DATE_UNIX_GMT = '1546012800';
const FALLS_DATE_UNIX_LOCAL = '1545973200';

const constants = {
  FALLS_DATE_JS_OBJ,
  FALLS_DATE_UNIX_GMT,
  FALLS_DATE_UNIX_LOCAL,
};

export default constants;
