import React, { Component } from 'react';

import Countdown from '../../components/countdown';
import styles from './styles.module.scss';

class HomePage extends Component {
  render() {
    return (
      <div className={styles.wrapper}>
        <Countdown />
      </div>
    );
  }
}

export default HomePage;
