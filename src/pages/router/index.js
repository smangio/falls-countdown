import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import HomePage from '../home';

const AppRouter = () => (
  <Router>
    <Route exact path="/" component={HomePage} />
  </Router>
);

AppRouter.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
};

export default AppRouter;
