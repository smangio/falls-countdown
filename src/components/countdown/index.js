import _ from 'lodash';
import React, { Component } from 'react';
import countdown from 'countdown';

import constants from '../../shared/data/constants';
import generateUnitArray from '../../shared/utils/generate-unit-array';
import styles from './styles.module.scss';

class Countdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: countdown(null, constants.FALLS_DATE_JS_OBJ),
      tentBitch: 'Victor',
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: countdown(null, constants.FALLS_DATE_JS_OBJ),
    });
  }

  render() {
    const { time } = this.state;
    const units = generateUnitArray(time);

    return (
      <div className={styles.countdown}>
        <h3>There are approximately</h3>
        <div className={styles.wrapper}>
          {_.map(units, unit => (
            <div className={styles.unitBlock}>
              <h1 className={styles.unit}>
                {unit.value}
              </h1>
              <small className={styles.unitText}>
                {unit.text}
              </small>
            </div>
          ))}
        </div>
        <h3 className={styles.text}>until <span className={styles.tentBitch}>{this.state.tentBitch}</span> becomes the tent bitch.</h3>
        <span role="img" aria-label="tent" className={styles.emoji}>🏕</span>
      </div>
    )
  }
}

export default Countdown;
