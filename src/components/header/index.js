import React from 'react';

import styles from './styles.module.scss';

const Header = () => (
  <div className={styles.header}>
    <p>
      This is a test header component.
    </p>
  </div>
);

export default Header;
